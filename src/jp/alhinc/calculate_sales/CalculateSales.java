package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.util.*;

public class CalculateSales {

	//定数の定義

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	//商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LIST = "commodity.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	//商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	//正規表現
	private static final String READ_BRANCHNAME_FILE_REGAX = "^[0-9]{3}$";
	private static final String READ_COMMODITY_FILE_REGAX = "^[0-9a-zA-Z]{8}$";


	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_CONTINUE = "売上ファイル名が連番になっていません";
	private static final String SALEFILE_INVALID_FORMAT = "のフォーマットが不正です";
	private static final String BRANCHFILE_INVALID_FORMAT_CODE = "の支店コードが不正です";
	private static final String COMMODITYFILE_INVALID_FORMAT_CODE = "の商品コードが不正です";
	private static final String OVERFLOW = "合計金額が10桁を超えました";
	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		//コマンドライン引数が入っているか確認
		if(args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		//商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		//商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();

		// 支店定義ファイル読み込み処理

		if(!readFile(args[0], FILE_NAME_BRANCH_LST, "支店" , READ_BRANCHNAME_FILE_REGAX, branchNames, branchSales)) {
			return;
		}

		//商品定義ファイル読み込み処理
		if(!readFile(args[0], FILE_NAME_COMMODITY_LIST, "商品",	READ_COMMODITY_FILE_REGAX, commodityNames, commoditySales)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		//指定したディレクトリの中のファイルをすべて配列filesに格納
		File[] files = new File(args[0]).listFiles();
		//ファイル格納用のリストrcdFiles
		List<File> rcdFiles = new ArrayList<>();

		for(int i = 0; i < files.length; i++) {
			//ファイル名のフォーマット確認
			if(files[i].isFile() && files[i].getName().matches("^[0-9]{8}.rcd$")) {
				rcdFiles.add(files[i]);
			}
		}
		//ファイルのソート
		Collections.sort(rcdFiles);

		for(int i = 0; i <rcdFiles.size() - 1 ; i ++) {
			int former = Integer.parseInt(rcdFiles.get(i).getName().substring(0, 8));
			int latter = Integer.parseInt(rcdFiles.get(i + 1).getName().substring(0, 8));
			//ファイルが連番で保持されているかどうかを確認する
			if(latter - former != 1) {
				System.out.println(FILE_NOT_CONTINUE);
				return;
			}
		}
		BufferedReader br = null;
		//支店別・商品別集計
		for(int i = 0; i < rcdFiles.size(); i ++) {
			try {
				FileReader rcdRead = new FileReader(rcdFiles.get(i));
				br = new BufferedReader(rcdRead);
				String rLine;
				List<String>rcdItems = new ArrayList<String>();

				while((rLine = br.readLine()) != null){
					rcdItems.add(rLine);
				}
				//売上ファイルがフォーマット通りの3要素で構成されているか確認
				if(rcdItems.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + SALEFILE_INVALID_FORMAT);
					return;
				}
				//支店定義にない支店コードがないか確認
				if(!branchNames.containsKey(rcdItems.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + BRANCHFILE_INVALID_FORMAT_CODE);
					return;
				}
				//商品定義にない商品コードがないか確認
				if(!commodityNames.containsKey(rcdItems.get(1))) {
					System.out.println(rcdFiles.get(i).getName() + COMMODITYFILE_INVALID_FORMAT_CODE);
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				Long fileSale = Long.parseLong(rcdItems.get(2));
				Long saleAmount = branchSales.get(rcdItems.get(0)) + fileSale;
				//10桁を超える金額ではないか確認
				if(saleAmount >= 10000000000L) {
					System.out.println(OVERFLOW);
					return;
				}
				branchSales.put(rcdItems.get(0), saleAmount);
				commoditySales.put(rcdItems.get(1), saleAmount);
			}catch(IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return ;

			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}
		// 支店別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}
		// 商品別集計ファイル書き込み処理
		if(!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}
	}
	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, String errorMessage,
			String regax, Map<String, String> nameMap, Map<String, Long> saleMap) {
		BufferedReader br = null;

		try {
			File definitionFile = new File(path, fileName);
			//定義ファイルがあるか確認
			if(!definitionFile.exists()) {
				System.out.println(errorMessage + FILE_NOT_EXIST);
				return false;
			}
			//ファイルの読み込み処理
			FileReader fr = new FileReader(definitionFile);
			br = new BufferedReader(fr);
			String line;

			// 一行ずつ読み込む
			while((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				//System.out.println(line);

				String[] items = line.split(",");

				//要素数が2かつ、引数で受け取った正規表現でフォーマット確認
				if((items.length != 2) || (!items[0].matches(regax))){
					System.out.println(errorMessage + FILE_INVALID_FORMAT);
					return false;
				}
				nameMap.put(items[0], items[1]);
				saleMap.put(items[0], 0L);
			}
		} catch(IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if(br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch(IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> nameMap,
			Map<String, Long> saleMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		//コマンドライン引数から受け取ったファイルを指定
		File writeFile = new File(path, fileName);
		BufferedWriter bw = null;

		try {
			//ファイル書き出し
			FileWriter fw = new FileWriter(writeFile);
			bw = new BufferedWriter(fw);

			//変数key に、引数で受け取った定義ファイルのキーをセット
			for(String key : nameMap.keySet()) {
				//定義ファイルのキーをもとに、キー、名称、売上を書き出す
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		}catch(IOException e) {
			System.out.println(e);
			return false;
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println(e);
					return false;
				}
			}
		}
		return true;
	}
}
